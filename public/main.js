'use strict';

var color;
var thickness = 3;
var x1 = 0;
var y1 = 0;
var x2 = 0;
var y2 = 0;
var drawing = false;
var canvas;
var context;

var id;

$(() => {
  canvas = $('canvas')[0];
  context = canvas.getContext('2d');
  canvas.addEventListener('mousemove', e => getCoordinates('move', e), false);
  canvas.addEventListener('mouseup', e => getCoordinates('up', e), false);
  canvas.addEventListener('mousedown', e => getCoordinates('down', e), false);
  canvas.addEventListener('touchmove', e => getCoordinates('tmove', e), false);
  canvas.addEventListener('touchend', e => getCoordinates('tup', e), false);
  canvas.addEventListener('touchstart', e => getCoordinates('tdown', e), false);
});

var socket = io();

socket.on('updateClients', clients => {
  var newClients = '';
  clients.forEach(client => {
    if(client.id != id) newClients += '<div class=client style=background:' + client.color + '><img class=icon src="icons/' + client.icon + '.png"><span class=tooltip>' + client.icon + '</span></div>';
  })
  $('#others').html(newClients);
});

socket.on('client', client => {
  $('#primary').html('<img class=icon src="icons/' + client.icon + '.png"><span class=tooltip>' + client.icon + ' (You)</span>').css('background', client.color);
  id = client.id;
  color = client.color;
  $('#color').val(color);
});

socket.on('getLines', lines => lines.forEach(line => draw(false, line.x1, line.y1, line.x2, line.y2, line.color, line.thickness)));

socket.on('getErase', () => context.clearRect(0, 0, canvas.width, canvas.height));

function getCoordinates(action, e) {
  if(action == 'down' || action == 'tdown') {
    drawing = true;
    if(action == 'down') {
      x2 = e.clientX - canvas.offsetLeft + document.documentElement.scrollLeft;
      y2 = e.clientY - canvas.offsetTop + document.documentElement.scrollTop;
    }
    else {
      var touch = e.targetTouches[0];
			x2 = touch.pageX - canvas.offsetLeft;
			y2 = touch.pageY - canvas.offsetTop;
			e.preventDefault();
    }
    x1 = x2;
    y1 = y2;
    draw(true, x1, y1, x2, y2, color, thickness);
  }
  if(action == 'up' || action == 'tup') drawing = false;
  if((action == 'move' || action == 'tmove') && drawing) {
    x1 = x2;
    y1 = y2;
    if(action == 'move') {
      x2 = e.clientX - canvas.offsetLeft + document.documentElement.scrollLeft;
      y2 = e.clientY - canvas.offsetTop + document.documentElement.scrollTop;
    }
    else {
      var touch = e.targetTouches[0];
			x2 = touch.pageX - canvas.offsetLeft;
			y2 = touch.pageY - canvas.offsetTop;
			e.preventDefault();
    }
    draw(true, x1, y1, x2, y2, color, thickness);
  }
}

function draw(transmit, x1, y1, x2, y2, color, thickness) {
  if(transmit) socket.emit('sendLine', {x1: x1, y1: y1, x2: x2, y2: y2, color: color, thickness: thickness});
  context.beginPath();
  context.moveTo(x1, y1);
  context.lineTo(x2, y2);
  context.strokeStyle = color;
  context.lineWidth = thickness;
  context.lineCap = 'round';
  context.stroke();
  context.closePath();
}

function changeThickness(increase) {
  if(increase) thickness++;
  else thickness--;
  if(thickness < 1) thickness = 50;
  if(thickness > 50) thickness = 1;
  $('#thicknessIndicator').text(thickness);
}

function changeColor() {
  color = $('#color').val();
  socket.emit('changeColor', color);
}

function erase() {
  if(window.confirm('Are you sure you want to erase the board?')) {
    socket.emit('sendErase');
    context.clearRect(0, 0, canvas.width, canvas.height);
  }
}
