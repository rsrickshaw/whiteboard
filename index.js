var express = require('express');
var app = express();
var fs = require('fs');
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static('public'));
app.set('port', process.env.PORT || 3000);
app.get('/', (req, res) => req.render('index.html'));

var lines = [];

if(!fs.existsSync('lines.json')) fs.writeFileSync('lines.json', '[]');
fs.readFile('lines.json', (err, data) => lines = JSON.parse(data));

var icons = 'Alligator,Anteater,Armadillo,Auroch,Axolotl,Badger,Bat,Beaver,Buffalo,Camel,Chameleon,Cheetah,Chipmunk,Chinchilla,Chupacabra,Cormorant,Coyote,Crow,Dingo,Dinosaur,Dolphin,Duck,Elephant,Ferret,Fox,Frog,Giraffe,Gopher,Grizzly,Hedgehog,Hippo,Hyena,Jackal,Ibex,Ifrit,Iguana,Koala,Kraken,Lemur,Leopard,Liger,Llama,Manatee,Mink,Monkey,Narwhal,Nyan Cat,Orangutan,Otter,Panda,Penguin,Platypus,Python,Pumpkin,Quagga,Rabbit,Raccoon,Rhino,Sheep,Shrew,Skunk,Slow Loris,Squirrel,Turtle,Walrus,Wolf,Wolverine,Wombat'.split(',');

var clients = [];

var lastIdIssued = 0;

function getRandomColor() {
  var color = '#';
  for(var i = 0; i < 6; i++) color += '0123456789ABCDEF'[Math.floor(Math.random() * 16)];
  return color;
}

io.on('connection', socket => {
	var client = {icon: icons[Math.floor(Math.random() * icons.length)], color: getRandomColor(), id: ++lastIdIssued}
	clients.push(client);
	socket.emit('client', client);
	socket.emit('getLines', lines);
	socket.emit('updateClients', clients);
	socket.broadcast.emit('updateClients', clients);

	socket.on('sendLine', line => {
		lines.push(line);
    fs.writeFileSync('lines.json', JSON.stringify(lines));
		socket.broadcast.emit('getLines', [line]);
	});

  socket.on('sendErase', () => {
    lines = [];
    fs.writeFileSync('lines.json', JSON.stringify(lines));
    socket.broadcast.emit('getErase');
  });

	socket.on('disconnect', () => {
		clients.splice(clients.indexOf(client), 1);
		socket.broadcast.emit('updateClients', clients);
	});

  socket.on('changeColor', color => {
    clients.splice(clients.indexOf(client), 1);
    client.color = color;
    socket.emit('client', client);
    clients.push(client);
    socket.broadcast.emit('updateClients', clients);
  });
});

http.listen(app.get('port'));
